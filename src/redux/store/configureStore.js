import { createStore } from 'redux';
import reducer from '../reducer';

const initialState = {
  menuName: '',
};

const configureStore = () => {
  return createStore(
    reducer,
    initialState,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() // 使redux-devtools生效
  );
};

export default configureStore;
