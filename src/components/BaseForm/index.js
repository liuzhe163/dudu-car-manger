import React from 'react';
import { Form, Input, Select, Button, Checkbox, DatePicker } from 'antd';

const FormItem = Form.Item;
const { Option } = Select;
const { RangePicker } = DatePicker;

class BaseItem extends React.Component {
  render() {
    const { item } = this.props;
    return (
      <FormItem name={item.field} label={item.label}>
        {this.props.children}
      </FormItem>
    );
  }
}

export default class BaseForm extends React.Component {
  formRef = React.createRef();
  submit = () => {
    var formData = this.formRef.current.getFieldsValue();
    this.props.onFormData(formData);
  };
  reset = () => {
    this.formRef.current.resetFields();
  };
  render() {
    const { formList, initValue } = this.props;
    return (
      <Form ref={this.formRef} layout="inline" initialValues={{ ...initValue }}>
        {formList.map((item, index) => {
          if (item.type === 'INPUT') {
            return (
              <BaseItem item={item} key={index}>
                <Input placeholder={item.placeholder || '请输入'} />
              </BaseItem>
            );
          } else if (item.type === 'SELECT') {
            return (
              <BaseItem item={item} key={index}>
                <Select
                  style={{ width: item.width }}
                  placeholder={item.placeholder || '请选择'}
                >
                  {item.list.map((option, index) => {
                    return (
                      <Option key={option.id} value={option.id || index}>
                        {option.name}
                      </Option>
                    );
                  })}
                </Select>
              </BaseItem>
            );
          } else if (item.type === 'CHECKBOX') {
            return (
              <BaseItem item={item} key={index}>
                <Checkbox />
              </BaseItem>
            );
          } else if (item.type === 'RANGEPICKER') {
            return (
              <BaseItem item={item} key={index}>
                <RangePicker />
              </BaseItem>
            );
          } else {
            return <FormItem key={index}>xxx</FormItem>;
          }
        })}
        <FormItem>
          <Button type="primary" onClick={this.submit}>
            查询
          </Button>
          <Button onClick={this.reset}>重置</Button>
        </FormItem>
      </Form>
    );
  }
}
