import React from "react";
import Utils from "../../utils/utils";
import { Table } from "antd";
import "./index.less";

export default class ETable extends React.Component {
  state = {};

  onRowClick = (record, index) => {
      let rowSelection = this.props;
  }

  getOptions = () => {
    let p = this.props;
    const name_list = {
      订单编号: 170,
      车辆编号: 80,
      手机号码: 96,
      用户姓名: 70,
      密码: 70,
      运维区域: 300,
      车型: 42,
      故障编号: 76,
      代理商编码: 97,
      角色ID: 64,
    };
    if (p.colums && p.colums.length > 0) {
      p.colums.forEach((item) => {
        if (!item.title) {
          return;
        }
        if (!item.width) {
          if (
            item.title.indexOf("时间") > -1 &&
            item.title.indexOf("持续时间") < 0
          ) {
            item.width = 132;
          } else if (item.title.indexOf("图片") > -1) {
            item.width = 86;
          } else if (
            item.title.indexOf("权限") > -1 ||
            item.title.indexOf("负责城市") > -1
          ) {
            item.width = "40%";
            item.className = "text-left";
          } else {
            if (name_list[item.title]) {
              item.width = name_list[item.title];
            }
          }
        }
        item.bordered = true;
      });
    }
    const {selectedRowKeys} = p;
    const rowSelection = {
        type: 'radio',
        selectedRowKeys,
        onChange = this.onSelectChange,
        onSelect: (record, selected, selectedRows) => {
            console.log('...')
        },
        onSelectedAll: this.onSelectAll
    }
    let row_selection = p.rowSelection;
    // 当属性为false或者null时，说明没有单选或者复选列
    if(row_selection === false || row_selection === null || row_selection === undefined) {
        row_selection = false;
    }else if(row_selection === 'checkbox') {
        rowSelection.type = 'checkbox'
    }else {
        row_selection = 'radio'
    }
    return <Table
            className="card-wrap page-table"
            bordered
            {...this.props}
            rowSelection={row_selection? rowSelection : null}
            onRow={(record, index) => {
                onClick: ()=>{
                    if(!row_selection) {
                        return 
                    }
                    this.onRowClick(record,index)
                }
            }}
            />
  };

  render = () => {
    return <div>{this.getOptions()}</div>;
  };
}
