import React, { Component } from 'react';
import { Row, Col } from 'antd';
import './index.less';
import { connect } from 'react-redux';

import Util from '../../utils/utils';
import axios from '../../axios';

import logoSvg from '../../assets/logo-ant.svg';

class Header extends Component {
  state = {};
  componentWillMount() {
    this.setState({
      userName: 'admin',
    });

    setInterval(() => {
      let sysTime = Util.formateDate(new Date().getTime());
      this.setState({
        sysTime,
      });
    }, 1000);

    this.getWeatherAPIData();
  }

  // 获取天气
  getWeatherAPIData() {
    let city = '上海';
    axios
      .jsonp({
        url:
          'http://api.map.baidu.com/telematics/v3/weather?location=' +
          encodeURIComponent(city) +
          '&output=json&ak=3p49MVra6urFRGOT9s8UBWr2',
      })
      .then((res) => {
        if (res.status === 'success') {
          let data = res.results[0].weather_data[0];
          this.setState({
            dayPictureUrl: data.dayPictureUrl,
            weather: data.weather,
          });
        }
      });
  }

  render() {
    const { menuName, menuType } = this.props;
    return (
      <div className="header">
        <Row className="header-top">
          {menuType ? (
            <Col span={6} className="logo">
              <img src={logoSvg} />
              <span>Imooc 通用管理系统</span>
            </Col>
          ) : (
            ''
          )}
          <Col span={menuType ? 18 : 24}>
            <span>欢迎, {this.state.userName}</span>
            <a className="link" href="#">
              退出
            </a>
          </Col>
        </Row>
        <div>
          {menuType ? (
            ''
          ) : (
            <Row className="breadcrumb">
              <Col span={4} className="breadcrumb-title">
                {menuName || '首页'}
              </Col>
              <Col span={20} className="weather">
                <span className="date">{this.state.sysTime}</span>
                <span className="weather-img">
                  <img src={this.state.dayPictureUrl} alt="" />
                </span>
                <span className="weather-detail">{this.state.weather}</span>
              </Col>
            </Row>
          )}
        </div>
      </div>
    );
  }
}
// 把redux中的state通过父组件到子组件的方式传递当前组件使用
const mapStateToProps = (state) => {
  return {
    menuName: state.menuName,
  };
};
export default connect(mapStateToProps)(Header);
