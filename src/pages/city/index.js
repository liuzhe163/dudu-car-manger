import React from "react";
import { Card, Form, Select, Button, message, Table, notification } from "antd";

import "./city.less";

import Modal from "antd/lib/modal/Modal";

import axios from "../../axios/index";
import Utils from "../../utils/utils";
import BaseForm from "../../components/BaseForm/index";
const FormItem = Form.Item;
const { Option } = Select;

export default class City extends React.Component {
  state = {
    open_visible: false,
    list: [],
  };
  componentDidMount() {
    this.request();
  }

  params = {
    page: 1,
  };

  request = () => {
    axios.requestList(this, "/open_city", this.params);
  };

  getFormData = (formData) => {
    notification.success({
      message: "获取到baseForm回调",
      description: `formData=${JSON.stringify(formData)}`,
    });
  };

  submitOpen = () => {
    console.log("提交");
    const formData = this.formRef.current.getFieldsValue();
    console.log("res");
    console.log(formData);
    axios
      .ajax({
        url: "/city/open",
        data: {
          params: formData,
        },
      })
      .then((res) => {
        this.setState({
          open_visible: false,
        });
        message.success("开通成功");
        this.request();
      });
  };

  formRef = React.createRef();

  render() {
    const columns = [
      { title: "城市ID", dataIndex: "id" },
      { title: "城市名称", dataIndex: "name" },
      {
        title: "用车模式",
        dataIndex: "mode",
        render: (mode) => (mode === 1 ? "停车点" : "禁停区"),
      },
      {
        title: "运营模式",
        dataIndex: "op_mode",
        render: (op_mode) => (op_mode === 1 ? "自营" : "加盟"),
      },
      { title: "授权加盟商", dataIndex: "franchisee_name" },
      {
        title: "城市管理员",
        dataIndex: "city_admins",
        render: (array) => {
          return array
            .map((admin) => {
              return admin.user_name;
            })
            .join(",");
        },
      },
      { title: "城市开通时间", dataIndex: "open_time" },
      {
        title: "操作时间",
        dataIndex: "update_time",
        render: Utils.formateDate,
      },
      { title: "操作人", dataIndex: "sys_user_name" },
    ];

    const formList = [
      {
        type: "SELECT",
        label: "城市",
        width: 120,
        field: "city",
        list: [
          { name: "全部", id: "0" },
          { name: "北京", id: "1" },
          { name: "天津", id: "2" },
          { name: "深圳", id: "3" },
        ],
      },
      {
        type: "SELECT",
        label: "用车模式",
        width: 160,
        field: "mode",
        list: [
          { name: "全部", id: "0" },
          { name: "指定停车点模式", id: "1" },
          { name: "禁停区模式", id: "2" },
        ],
      },
      {
        type: "SELECT",
        label: "运营模式",
        width: 120,
        field: "op_mode",
        list: [
          { name: "全部", id: "0" },
          { name: "自营", id: "1" },
          { name: "加盟", id: "2" },
        ],
      },
      {
        type: "SELECT",
        label: "加盟商授权",
        width: 140,
        field: "auth_status",
        list: [
          { name: "全部", id: "0" },
          { name: "已授权", id: "1" },
          { name: "未授权", id: "2" },
        ],
      },
    ];
    const formInitValue = {
      city: "1",
      op_mode: "0",
      auth_status: "1",
    };
    return (
      <div className="card-wrap">
        <Card>
          <BaseForm
            formList={formList}
            initValue={formInitValue}
            onFormData={this.getFormData}
          />
        </Card>
        <div className="city-curd">
          <div>
            <Button
              className="open-button"
              type="primary"
              onClick={() => {
                this.setState({
                  open_visible: true,
                });
              }}
            >
              开通城市
            </Button>
          </div>
          <div>
            <Table
              columns={columns}
              bordered
              dataSource={this.state.list}
              pagination={this.state.pagination}
            />
          </div>
        </div>
        <Modal
          title="开通城市"
          visible={this.state.open_visible}
          onOk={this.submitOpen}
          onCancel={() => {
            this.setState({
              open_visible: false,
            });
          }}
        >
          <Form
            layout="horizontal"
            initialValues={{
              city_id: "",
              op_mode: "1",
              use_mode: "1",
            }}
            ref={this.formRef}
          >
            <FormItem label="选择城市" name="city_id">
              <Select style={{ width: 100 }}>
                <Option value="">全部</Option>
                <Option value="1">北京市</Option>
                <Option value="2">天津市</Option>
              </Select>
            </FormItem>
            <FormItem label="运营模式" name="op_mode">
              <Select style={{ width: 100 }}>
                <Option value="1">自营</Option>
                <Option value="2">加盟</Option>
              </Select>
            </FormItem>
            <FormItem label="用车模式" name="use_mode">
              <Select style={{ width: 120 }}>
                <Option value="1">指定停车点</Option>
                <Option value="2">禁停区</Option>
              </Select>
            </FormItem>
          </Form>
        </Modal>
      </div>
    );
  }
}
