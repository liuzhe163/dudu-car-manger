import React from 'react';
import { Form, Select, Button } from 'antd';
import './city.less';

const FormItem = Form.Item;
const { Option } = Select;

export default class FilterForm extends React.Component {
  render() {
    return (
      <Form layout="inline">
        <FormItem className="filterItem" label="城市" name="city">
          <Select placeholder="全部">
            <Option value="">全部</Option>
            <Option value="1">北京市</Option>
            <Option value="2">天津市</Option>
            <Option value="3">深圳市</Option>
          </Select>
        </FormItem>
        <FormItem
          className="filterItem wideSelect"
          label="用车模式"
          name="mode"
        >
          <Select placeholder="全部">
            <Option value="">全部</Option>
            <Option value="1">指定停车点模式</Option>
            <Option value="2">禁停区模式</Option>
          </Select>
        </FormItem>
        <FormItem className="filterItem" label="运营模式" name="op_mode">
          <Select placeholder="全部">
            <Option value="">全部</Option>
            <Option value="1">自营</Option>
            <Option value="2">加盟</Option>
          </Select>
        </FormItem>
        <FormItem className="filterItem" label="加盟商授权" name="auth_status">
          <Select placeholder="全部">
            <Option value="">全部</Option>
            <Option value="1">已授权</Option>
            <Option value="2">未授权</Option>
          </Select>
        </FormItem>
        <FormItem>
          <Button type="primary">查询</Button>
          <Button>重置</Button>
        </FormItem>
      </Form>
    );
  }
}
