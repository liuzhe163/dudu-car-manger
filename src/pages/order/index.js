import React from "react";
import { Card, Form, Button, Table, notification, Modal, message } from "antd";

import BaseForm from "../../components/BaseForm/index";
import Utils from "../../utils/utils";
import axios from "../../axios/index";
import "./order.less";

const FormItem = Form.Item;

export default class Order extends React.Component {
  state = {
    orderInfo: {},
    orderConfirmVisble: false,
  };

  componentDidMount() {
    this.request();
  }
  getFormData = (formData) => {
    notification.success({
      message: "获取到baseForm回调",
      description: `formData=${JSON.stringify(formData)}`,
    });
  };
  params = {
    page: 1,
  };
  // 配置单击表格
  onRowClick = (record, index) => {
    this.setState({
      selectedRowKeys: [record.id],
      selectedRows: record,
    });
  };
  // 两个操作按钮
  openOrderDetail = () => {
    let item = this.state.selectedRows;
    if (!item) {
      return Modal.info({
        title: "提示",
        content: "请先选择一条订单",
      });
    }
    window.open(`/#/order/detail?orderId=${item.id}`, "_blank");
  };
  handleConfirm = () => {
    let item = this.state.selectedRows;
    if (!item) {
      return Modal.info({
        title: "提示",
        content: "请先选择一条订单",
      });
    }
    axios
      .ajax({
        url: "/order/ebike_info",
        data: {
          params: {
            orderId: item.id,
          },
        },
      })
      .then((res) => {
        if (res.code === 0) {
          this.setState({
            orderInfo: res.result,
            orderConfirmVisble: true,
          });
        }
      });
  };
  // 结束订单确认
  handleFinishOrder = () => {
    let item = this.state.selectedRows;
    axios
      .ajax({
        url: "/order/finish_order",
        data: {
          params: {
            orderId: item.id,
          },
        },
      })
      .then((res) => {
        if (res.code === 0) {
          message.success("订单结束成功");
          this.setState({
            orderConfirmVisble: false,
          });
          this.request();
        }
      });
  };
  // 列表请求
  request = () => {
    axios.requestList(this, "/order/list", this.params);
  };
  render() {
    const formList = [
      {
        type: "SELECT",
        label: "城市",
        field: "city",
        width: 120,
        list: [
          { name: "全部", id: "0" },
          { name: "北京", id: "1" },
          { name: "天津", id: "2" },
          { name: "上海", id: "3" },
        ],
      },
      {
        type: "RANGEPICKER",
        field: "date",
      },
      {
        type: "SELECT",
        label: "订单状态",
        width: 140,
        field: "order_state",
        list: [
          { name: "全部", id: "0" },
          { name: "进行中", id: "1" },
          { name: "结束行程", id: "2" },
        ],
      },
    ];
    const formInitValue = {};
    const columns = [
      {
        title: "订单编号",
        dataIndex: "order_sn",
      },
      {
        title: "车辆编号",
        dataIndex: "bike_sn",
      },
      {
        title: "用户名",
        dataIndex: "user_name",
      },
      {
        title: "手机号",
        dataIndex: "mobile",
      },
      {
        title: "里程",
        dataIndex: "distance",
        render(distance) {
          return distance / 1000 + "Km";
        },
      },
      {
        title: "行驶时长",
        dataIndex: "total_time",
      },
      {
        title: "状态",
        dataIndex: "status",
      },
      {
        title: "开始时间",
        dataIndex: "start_time",
      },
      {
        title: "结束时间",
        dataIndex: "end_time",
      },
      {
        title: "订单金额",
        dataIndex: "total_fee",
      },
      {
        title: "实付金额",
        dataIndex: "user_pay",
      },
    ];
    const selectedRowKeys = this.state.selectedRowKeys;
    const rowSelection = {
      type: "radio",
      selectedRowKeys,
    };
    return (
      <div className="card-wrap">
        <Card>
          <BaseForm
            formList={formList}
            initValue={formInitValue}
            onFormData={this.getFormData}
          />
        </Card>
        <div className="content-wrap">
          <div className="order-options">
            <Button type="primary" onClick={this.openOrderDetail}>
              订单详情
            </Button>
            <Button
              type="primary"
              // style={{ marginLeft: 10 }}
              onClick={this.handleConfirm}
            >
              结束订单
            </Button>
          </div>
          <Table
            columns={columns}
            bordered
            dataSource={this.state.list}
            rowSelection={rowSelection}
            pagination={this.state.pagination}
            onRow={(record, index) => {
              return {
                onClick: () => {
                  this.onRowClick(record, index);
                },
              };
            }}
          ></Table>
        </div>
        <Modal
          title="结束订单"
          visible={this.state.orderConfirmVisble}
          width={600}
          onCancel={() => this.setState({ orderConfirmVisble: false })}
          onOk={this.handleFinishOrder}
        >
          <Form layout="horizontal">
            <FormItem label="车辆编号">{this.state.orderInfo.bike_sn}</FormItem>
            <FormItem label="剩余电量">
              {this.state.orderInfo.battery + "%"}
            </FormItem>
            <FormItem label="行程开始时间">
              {this.state.orderInfo.start_time}
            </FormItem>
            <FormItem label="当前位置">
              {this.state.orderInfo.location}
            </FormItem>
          </Form>
        </Modal>
      </div>
    );
  }
}
