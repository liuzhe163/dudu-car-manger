import React from 'react';
import { Card, Modal } from 'antd';
import axios from '../../axios/index';
import './detail.less';
import startImg from '../../assets/imgs/start_point.png';
import endImg from '../../assets/imgs/end_point.png';

export default class OrderDetail extends React.Component {
  state = {};
  componentDidMount() {
    // this.renderMap();
    const { hash } = window.location;
    const searchStr = hash.split('?')[1];
    if (searchStr) {
      const orderId = searchStr.split('=')[1] || '';
      if (orderId) {
        this.getDetailInfo(orderId);
      }
    } else {
      Modal.warning({
        title: '提示',
        content: '没有正在进行中的订单,请返回上一层',
        onOk: () => {
          if (window.history.length > 1) {
            window.history.go(-1);
          } else {
            window.close();
          }
        },
      });
    }
  }
  getDetailInfo = (orderId) => {
    axios
      .ajax({
        url: '/order/detail',
        data: {
          params: {
            orderId,
          },
        },
      })
      .then((res) => {
        console.log('res');
        console.log(res);
        if (res.code === 0) {
          const orderInfo = res.result;
          this.setState({ orderInfo });
          this.renderMap(orderInfo);
        }
      });
  };

  renderMap = (orderInfo) => {
    let map = new BMapGL.Map('orderDetailMap'); // 创建Map实例
    this.map = map;

    this.addMapControl();
    // 调用路线图绘制方法
    this.drawBikeRoute(orderInfo.position_list);
    // // 调用服务区绘制方法
    this.drawServiceArea(orderInfo.area);
  };
  // 添加地图控件
  addMapControl = () => {
    let { map } = this;
    // 添加比例尺控件
    map.addControl(new BMapGL.ScaleControl());
    // 添加缩放控件
    map.addControl(new BMapGL.ZoomControl());
  };

  // 路线绘制
  drawBikeRoute = (positionList) => {
    let map = this.map;
    let startPoint = '';
    let endPoint = '';
    if (positionList.length > 0) {
      let first = positionList[0];
      let last = positionList[positionList.length - 1];
      startPoint = new BMapGL.Point(first.lon, first.lat);
      let startIcon = new BMapGL.Icon(
        startImg,
        new BMapGL.Size(36, 42),
        {
          imageSize: new BMapGL.Size(36, 42),
          anchor: new BMapGL.Size(18, 42),
        }
      );

      let startMarker = new BMapGL.Marker(startPoint, { icon: startIcon });
      this.map.addOverlay(startMarker);

      endPoint = new BMapGL.Point(last.lon, last.lat);
      let endIcon = new BMapGL.Icon(
        endImg,
        new BMapGL.Size(36, 42),
        {
          imageSize: new BMapGL.Size(36, 42),
          anchor: new BMapGL.Size(18, 42),
        }
      );
      let endMarker = new BMapGL.Marker(endPoint, { icon: endIcon });
      this.map.addOverlay(endMarker);

      // 连接路线图
      let trackPoint = [];
      for (let i = 0; i < positionList.length; i++) {
        let point = positionList[i];
        trackPoint.push(new BMapGL.Point(point.lon, point.lat));
      }

      let polyline = new BMapGL.Polyline(trackPoint, {
        strokeColor: '#1869AD',
        strokeWeight: 3,
        strokeOpacity: 1,
      });
      this.map.addOverlay(polyline);
      this.map.centerAndZoom(endPoint, 11);
    }
  };
  // 服务区域绘制
  drawServiceArea = (positionList) => {
    // 连接路线图
    let trackPoint = [];
    for (let i = 0; i < positionList.length; i++) {
        let point = positionList[i];
        trackPoint.push(new BMapGL.Point(point.lon, point.lat));
    }
    // 绘制服务区
    let polygon = new BMapGL.Polygon(trackPoint, {
        strokeColor: '#CE0000',
        strokeWeight: 4,
        strokeOpacity: 1,
        fillColor: '#ff8605',
        fillOpacity:0.4
    })
    this.map.addOverlay(polygon);
  };

  render() {
    const info = this.state.orderInfo || {};
    return (
      <div className="card-wrap">
        <Card>
          <div id="orderDetailMap" className="order-map"></div>
          <div className="detail-items">
            <div className="item-title">基础信息</div>
            <ul className="detail-form">
              <li>
                <div className="detail-form-left">用车模式</div>
                <div className="detail-form-content">
                  {info.mode == 1 ? '服务区' : '停车点'}
                </div>
              </li>
              <li>
                <div className="detail-form-left">订单编号</div>
                <div className="detail-form-content">{info.order_sn}</div>
              </li>
              <li>
                <div className="detail-form-left">车辆编号</div>
                <div className="detail-form-content">{info.bike_sn}</div>
              </li>
              <li>
                <div className="detail-form-left">用户姓名</div>
                <div className="detail-form-content">{info.user_name}</div>
              </li>
              <li>
                <div className="detail-form-left">手机号码</div>
                <div className="detail-form-content">{info.mobile}</div>
              </li>
            </ul>
          </div>
          <div className="detail-items">
            <div className="item-title">行驶轨迹</div>
            <ul className="detail-form">
              <li>
                <div className="detail-form-left">行程起点</div>
                <div className="detail-form-content">{info.start_location}</div>
              </li>
              <li>
                <div className="detail-form-left">行程终点</div>
                <div className="detail-form-content">{info.end_location}</div>
              </li>
              <li>
                <div className="detail-form-left">行驶里程</div>
                <div className="detail-form-content">
                  {info.distance / 1000}公里
                </div>
              </li>
            </ul>
          </div>
        </Card>
      </div>
    );
  }
}
