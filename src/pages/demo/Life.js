import React, {Component} from 'react'
import Child from './Child'

export default class Life extends Component {

  constructor(props){
    super(props)
    this.state = {
      count: 0
    }
  }

  // 等价于上边写在constructor中
  // state = {
  //   count: 0
  // }

  commonAdd = () => {
    this.setState({
      count: this.state.count + 1
    })
  }


  handleAdd1(){
    this.commonAdd()
  }

  handleAdd2 = () => {
    this.commonAdd()
  }

  render(){
    const style = {
      padding: 30
    }
    return (
      <div className="p50">
        <Child name={this.state.count} />
        <p>React生命周期介绍</p>
        <button onClick={this.handleAdd1.bind(this)}>点一下(使用bind this绑定)</button>
        <button onClick={this.handleAdd2}>点一下</button>
        <p>{this.state.count}</p>
      </div>
    )
  }
}

