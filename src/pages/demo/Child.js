
import React, {Component} from 'react'

export default class Child extends Component {

  constructor(props){
    super(props)
  }

  componentWillMount(){
    console.log('will Mount');
  }

  componentDidMount(){
    console.log('did Mount');
  }

  componentWillReceiveProps(newProps){
    console.log('will receive props' + newProps);
  }

  // 一旦调用了setState就会执行
  shouldComponentUpdate(){
    console.log('should update');
    return true   // 不返回true就不会往下执行
  }

  componentWillUpdate(){
    console.log('will update');
  }

  componentDidUpdate(){
    console.log('did Update');
  }

  render(){
    return (
      <div>
        <p>child component</p>
      </div>
    )
  }

}