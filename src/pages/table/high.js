import React from 'react';
import { Table, Card, Button, Badge, Modal, message } from 'antd';

import axios from '../../axios/index';
export default class HighTable extends React.Component {
  state = {};
  param = {
    page: 1,
  };
  componentDidMount() {
    this.request();
  }
  request = () => {
    axios
      .ajax({
        url: '/table/high',
        data: {
          params: {
            page: this.param.page,
          },
          loading: false,
        },
      })
      .then((data) => {
        if (data.code === 0) {
          const list = data.result.list.map((item) => {
            return { ...item, key: item.id };
          });
          this.setState({
            dataSource: list,
          });
        }
      });
  };

  handleDelete = (item) => {
    let id = item.id;
    Modal.confirm({
      title: '确认',
      content: '您确认要删除此条数据吗？',
      onOk: () => {
        message.success('删除成功');
        this.request();
      },
    });
  };
  render() {
    const columns = [
      { title: '姓名', dataIndex: 'username', key: 'username', width: 80 },
      {
        title: '性别',
        dataIndex: 'sex',
        key: 'sex',
        width: 80,
        render(sex) {
          return sex === 1 ? '男' : '女';
        },
      },
      {
        title: '状态',
        dataIndex: 'state',
        key: 'state',
        width: 80,
        render(state) {
          let config = {
            1: '咸鱼一条',
            2: '风华浪子',
            3: '北大才子',
            4: '百度FE',
            5: '创业者',
          };
          return config[state];
        },
      },
      {
        title: '兴趣',
        width: 80,
        dataIndex: 'interest',
        key: 'interest',
        render(state) {
          let config = {
            1: '打球',
            2: '爬山',
            3: '跑步',
            4: '骑行',
            5: '游泳',
            6: '踢足球',
            7: '买吧',
            8: '散步',
          };
          return config[state];
        },
      },
      {
        title: '是否结婚',
        width: 80,
        dataIndex: 'isMarried',
        key: 'isMarried',
        render: (gender) => (gender === 1 ? '是' : '否'),
      },
      { title: '出生年月', width: 120, dataIndex: 'birthday', key: 'birthday' },
      { title: '地址', width: 120, dataIndex: 'address', key: 'address' },
      { title: '起床时间', width: 120, dataIndex: 'time', key: 'time' },
    ];
    const columns2 = [
      {
        title: '姓名',
        dataIndex: 'username',
        key: 'username',
        width: 80,
        fixed: 'left',
      },
      {
        title: '性别',
        dataIndex: 'sex',
        key: 'sex',
        width: 80,
        fixed: 'left',
        render(sex) {
          return sex === 1 ? '男' : '女';
        },
      },
      {
        title: '状态',
        dataIndex: 'state',
        key: 'state',
        width: 140,
        render(state) {
          let config = {
            1: '咸鱼一条',
            2: '风华浪子',
            3: '北大才子',
            4: '百度FE',
            5: '创业者',
          };
          return config[state];
        },
      },
      {
        title: '兴趣',
        width: 80,
        dataIndex: 'interest',
        key: 'interest',
        render(state) {
          let config = {
            1: '打球',
            2: '爬山',
            3: '跑步',
            4: '骑行',
            5: '游泳',
            6: '踢足球',
            7: '买吧',
            8: '散步',
          };
          return config[state];
        },
      },
      {
        title: '是否结婚',
        width: 80,
        dataIndex: 'isMarried',
        key: 'isMarried',
        render: (gender) => (gender === 1 ? '是' : '否'),
      },
      { title: '出生年月', width: 120, dataIndex: 'birthday', key: 'birthday' },
      { title: '出生年月', width: 120, dataIndex: 'birthday', key: 'birthday' },
      { title: '出生年月', width: 120, dataIndex: 'birthday', key: 'birthday' },
      { title: '出生年月', width: 120, dataIndex: 'birthday', key: 'birthday' },
      { title: '出生年月', width: 120, dataIndex: 'birthday', key: 'birthday' },
      { title: '出生年月', width: 120, dataIndex: 'birthday', key: 'birthday' },
      { title: '出生年月', width: 120, dataIndex: 'birthday', key: 'birthday' },
      { title: '出生年月', width: 120, dataIndex: 'birthday', key: 'birthday' },
      {
        title: '地址',
        fixed: 'right',
        width: 120,
        dataIndex: 'address',
        key: 'address',
      },
      {
        title: '起床时间',
        fixed: 'right',
        width: 120,
        dataIndex: 'time',
        key: 'time',
      },
    ];
    const columns3 = [
      { title: '姓名', dataIndex: 'username', key: 'username', fixed: 'left' },
      {
        title: '性别',
        dataIndex: 'sex',
        key: 'sex',
        fixed: 'left',
        render(sex) {
          return sex === 1 ? '男' : '女';
        },
      },
      {
        title: '年龄',
        dataIndex: 'age',
        key: 'age',
        sorter: (a, b) => b.age - a.age,
      },
      {
        title: '状态',
        dataIndex: 'state',
        key: 'state',
        render(state) {
          let config = {
            1: '咸鱼一条',
            2: '风华浪子',
            3: '北大才子',
            4: '百度FE',
            5: '创业者',
          };
          return config[state];
        },
      },
      {
        title: '兴趣',
        dataIndex: 'interest',
        key: 'interest',
        render(state) {
          let config = {
            1: '打球',
            2: '爬山',
            3: '跑步',
            4: '骑行',
            5: '游泳',
            6: '踢足球',
            7: '买吧',
            8: '散步',
          };
          return config[state];
        },
      },
      {
        title: '是否结婚',
        dataIndex: 'isMarried',
        key: 'isMarried',
        render: (gender) => (gender === 1 ? '是' : '否'),
      },
      { title: '出生年月', dataIndex: 'birthday', key: 'birthday' },
      { title: '地址', fixed: 'right', dataIndex: 'address', key: 'address' },
      { title: '起床时间', fixed: 'right', dataIndex: 'time', key: 'time' },
    ];
    const columns4 = [
      { title: '姓名', dataIndex: 'username', key: 'username', fixed: 'left' },
      {
        title: '性别',
        dataIndex: 'sex',
        key: 'sex',
        fixed: 'left',
        render(sex) {
          return sex === 1 ? '男' : '女';
        },
      },
      {
        title: '年龄',
        dataIndex: 'age',
        key: 'age',
      },
      {
        title: '状态',
        dataIndex: 'state',
        key: 'state',
        render(state) {
          let config = {
            1: <Badge status="success" text="成功" />,
            2: <Badge status="error" text="报错" />,
            3: <Badge status="default" text="正常" />,
            4: <Badge status="processing" text="进行中" />,
            5: <Badge status="warning" text="警告" />,
          };
          return config[state];
        },
      },
      {
        title: '兴趣',
        dataIndex: 'interest',
        key: 'interest',
        render(state) {
          let config = {
            1: '打球',
            2: '爬山',
            3: '跑步',
            4: '骑行',
            5: '游泳',
            6: '踢足球',
            7: '买吧',
            8: '散步',
          };
          return config[state];
        },
      },
      {
        title: '是否结婚',
        dataIndex: 'isMarried',
        key: 'isMarried',
        render: (gender) => (gender === 1 ? '是' : '否'),
      },
      { title: '出生年月', dataIndex: 'birthday', key: 'birthday' },
      { title: '地址', fixed: 'right', dataIndex: 'address', key: 'address' },
      {
        title: '操作',
        fixed: 'right',
        render: (text, item) => {
          return (
            <Button size="small" onClick={(item) => this.handleDelete(item)}>
              删除
            </Button>
          );
        },
      },
    ];
    return (
      <div className="card-wrap">
        <Card title="头部固定">
          <Table
            bordered
            dataSource={this.state.dataSource}
            columns={columns}
            pagination={false}
            scroll={{ y: 240 }}
          />
        </Card>
        <Card title="两边固定">
          <Table
            bordered
            dataSource={this.state.dataSource}
            columns={columns2}
            pagination={false}
            scroll={{ x: 1620 }}
          />
        </Card>
        <Card title="表格排序">
          <Table
            bordered
            dataSource={this.state.dataSource}
            columns={columns3}
            pagination={false}
          />
        </Card>
        <Card title="按钮操作">
          <Table
            bordered
            dataSource={this.state.dataSource}
            columns={columns4}
            pagination={false}
          />
        </Card>
      </div>
    );
  }
}
