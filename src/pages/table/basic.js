import React from 'react';
import { Table, Card, Button, notification, Modal } from 'antd';

import Utils from '../../utils/utils';

import axios from '../../axios/index';
export default class BasicTable extends React.Component {
  state = {
    selectedCheckRowKeys: [], // 多选的key数组
    selectedCheckRows: [], // 多选的record数据
  };
  param = {
    page: 1,
  };
  componentDidMount() {
    const dataSource = [
      { key: '1', name: '胡彦斌', age: 32, address: '西湖区湖底公园1号' },
      { key: '2', name: '胡彦祖', age: 42, address: '西湖区湖底公园1号' },
    ];

    this.setState({
      dataSource,
    });
    this.request();
  }
  request = () => {
    axios
      .ajax({
        url: '/table/list',
        data: {
          params: {
            page: this.param.page,
          },
          loading: false,
        },
      })
      .then((data) => {
        if (data.code === 0) {
          const list = data.result.list.map((item) => {
            return { ...item, key: item.id };
          });
          this.setState({
            dataSource2: list,
            pagination: Utils.pagination(data, (current) => {
              this.param.page = current;
              this.request();
            }),
          });
        }
      });
  };
  onRowClick = (record) => {
    notification.success({
      message: '提示',
      description: `选中了${record.username},兴趣是${record.interest}`,
    });
    this.setState({
      selectedRowKeys: [record.key],
      selectedItem: record,
    });
  };

  onCheckRowClick = (record) => {
    let { id } = record;
    let { selectedCheckRowKeys, selectedCheckRows } = this.state;
    let list = selectedCheckRowKeys.slice();
    let rowsList = selectedCheckRows.slice();
    let index = selectedCheckRowKeys.indexOf(id);
    if (index === -1) {
      list.push(id);
      rowsList.push(record);
    } else {
      list.splice(index, 1);
      rowsList.slice(index, 1);
    }
    this.setState({
      selectedCheckRowKeys: list,
      selectedCheckRows: rowsList,
    });
  };

  deleteRows = () => {
    let { selectedCheckRowKeys, selectedCheckRows } = this.state;

    console.log('selectedCheckRowKeys');
    console.log(selectedCheckRowKeys);
    console.log('selectedCheckRows');
    console.log(selectedCheckRows);
    Modal.confirm({
      title: '确认?',
      content: `你确定要删除这些${selectedCheckRowKeys.join('-')}`,
      onOk: () => {
        this.setState({
          selectedCheckRowKeys: [],
          selectedCheckRows: [],
        });
      },
    });
  };

  render() {
    const columns = [
      { title: '姓名', dataIndex: 'name', key: 'name' },
      { title: '年龄', dataIndex: 'age', key: 'age' },
      { title: '住址', dataIndex: 'address', key: 'address' },
    ];
    const columns2 = [
      { title: '姓名', dataIndex: 'username', key: 'username' },
      {
        title: '性别',
        dataIndex: 'sex',
        key: 'sex',
        render(sex) {
          return sex === 1 ? '男' : '女';
        },
      },
      {
        title: '状态',
        dataIndex: 'state',
        key: 'state',
        render(state) {
          let config = {
            1: '咸鱼一条',
            2: '风华浪子',
            3: '北大才子',
            4: '百度FE',
            5: '创业者',
          };
          return config[state];
        },
      },
      {
        title: '兴趣',
        dataIndex: 'interest',
        key: 'interest',
        render(state) {
          let config = {
            1: '打球',
            2: '爬山',
            3: '跑步',
            4: '骑行',
            5: '游泳',
            6: '踢足球',
            7: '买吧',
            8: '散步',
          };
          return config[state];
        },
      },
      {
        title: '是否结婚',
        dataIndex: 'isMarried',
        key: 'isMarried',
        render: (gender) => (gender === 1 ? '是' : '否'),
      },
      { title: '出生年月', dataIndex: 'birthday', key: 'birthday' },
      { title: '地址', dataIndex: 'address', key: 'address' },
      { title: '起床时间', dataIndex: 'time', key: 'time' },
    ];
    const rowSelection = {
      type: 'radio',
      selectedRowKeys: this.state.selectedRowKeys,
    };
    const rowCheckSelection = {
      type: 'checkbox',
      selectedRowKeys: this.state.selectedCheckRowKeys,
      onChange: (selectedRowKeys, selectedRows) => {
        this.setState({
          selectedCheckRowKeys: selectedRowKeys,
          selectedCheckRows: selectedRows,
        });
      },
    };
    return (
      <div className="card-wrap">
        <Card title="基础表格">
          <Table
            bordered
            dataSource={this.state.dataSource}
            columns={columns}
            pagination={false}
          />
        </Card>
        <Card title="动态渲染mock数据">
          <Table
            bordered
            dataSource={this.state.dataSource2}
            columns={columns2}
            pagination={false}
          />
        </Card>
        <Card title="表格选择-单选">
          <Table
            rowSelection={rowSelection}
            onRow={(record) => {
              return {
                onClick: (event) => {
                  this.onRowClick(record);
                }, // 点击行
              };
            }}
            bordered
            dataSource={this.state.dataSource2}
            columns={columns2}
            pagination={false}
          />
        </Card>
        <Card title="表格选择-多选">
          <div style={{ marginBottom: 10 }}>
            <Button onClick={this.deleteRows}>删除选中</Button>
          </div>
          <Table
            rowSelection={rowCheckSelection}
            onRow={(record) => {
              return {
                onClick: (event) => {
                  this.onCheckRowClick(record);
                }, // 点击行
              };
            }}
            bordered
            dataSource={this.state.dataSource2}
            columns={columns2}
            pagination={false}
          />
        </Card>
        <Card title="表格-分页">
          <Table
            bordered
            dataSource={this.state.dataSource2}
            columns={columns2}
            pagination={this.state.pagination}
          />
        </Card>
      </div>
    );
  }
}
