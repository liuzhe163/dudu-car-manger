import React from 'react'
import { Row, Col, Card, Modal } from 'antd'

import './ui.less'

export default class Gallery extends React.Component {

  state = {
    modalImg: null,
    modalVisible: false
  }

  openModal = (imgSrc) => {
    this.setState({
      modalVisible: true,
      modalImg: imgSrc
    })
  }

  render() {
    const imgs = [
      ['1.png', '2.png', '3.png', '4.png', '5.png'],
      ['6.png', '7.png', '8.png', '9.png', '10.png'],
      ['11.png', '12.png', '13.png', '14.png', '15.png'],
      ['16.png', '17.png', '18.png', '19.png', '20.png'],
      ['21.png', '22.png', '23.png', '24.png', '25.png'],
    ]


    const imgList = imgs.map((list, listIndex) => {
      return (
        <Col key={listIndex} span={imgs.length - 1 === listIndex ? 4 : 5}>
          {list.map((item, itemIndex) => {
            return (
              <Card
                style={{ marginBottom: 10 }}
                key={itemIndex}
                onClick={() => this.openModal(require(`../../assets/gallery/${item}`))}
                cover={
                  <img src={require(`../../assets/gallery/${item}`)} />
                }
              >
                <Card.Meta title="React & AntD" description="I Love React" />
              </Card>
            )
          })}
        </Col>)
    })
    return (
      <div class="gallery-wrap">
        <Row gutter={10}>
          {imgList}
        </Row>
        <Modal
          onCancel={() => this.setState({ modalVisible: false })}
          visible={this.state.modalVisible}
          width={300}
          height={500}
          footer={null}
          title="图片画廊">
          <img src={this.state.modalImg} alt="modalImg" style={{ width: '100%' }} />
        </Modal>
      </div>
    )
  }
}