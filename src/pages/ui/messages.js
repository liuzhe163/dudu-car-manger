import React from 'react'
import { Button, Card, Space, message } from 'antd'

import './ui.less'

export default class Messages extends React.Component {

  openMessages = type => {
    message[type]('React,进阶成功!')
  }

  render() {
    return (
      <div className="card-wrap">
        <Card title="全局提示框">
          <Space>
            <Button type="primary" onClick={ ()=> this.openMessages('success')}>Success</Button>
            <Button type="primary" onClick={ ()=> this.openMessages('info')}>Info</Button>
            <Button type="primary" onClick={ ()=> this.openMessages('warning')}>Warning</Button>
            <Button type="primary" onClick={ ()=> this.openMessages('error')}>Error</Button>
            <Button type="primary" onClick={ ()=> this.openMessages('loading')}>Loading</Button>
          </Space>
        </Card>
      </div>
    )
  }
}