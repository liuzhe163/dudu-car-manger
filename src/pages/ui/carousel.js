import React from 'react'
import { Carousel, Card } from 'antd'

import './ui.less'

export default class ICarousel extends React.Component {

  render() {
    return (
      <div class="card-wrap">
        <Card title="文字轮播">
          <div class="carousel-wrap">
            <Carousel>
              <div className="carousel-div">
                <h3>React is a good lib</h3>
              </div>
              <div className="carousel-div">
                <h3>Vue is a good lib</h3>
              </div>
              <div className="carousel-div">
                <h3>Angular is a good lib</h3>
              </div>
            </Carousel>
          </div>
        </Card>
        <Card title="图片轮播">
          <div class="carousel-wrap carousel-img-wrap">
            <Carousel autoplay>
              <div className="carousel-div">
                <img src={require('../../assets/carousel-img/carousel-1.jpg')} alt=""/>
              </div>
              <div className="carousel-div">
                <img src={require('../../assets/carousel-img/carousel-2.jpg')} alt=""/>
              </div>
              <div className="carousel-div">
                <img src={require('../../assets/carousel-img/carousel-3.jpg')} alt=""/>
              </div>
            </Carousel>
          </div>
        </Card>
      </div>
    )
  }
}