import React from 'react'
import { Button, Card, notification, Space, message } from 'antd'

import './ui.less'

export default class Notice extends React.Component {

  openNotification = type => {
    notification[type]({
      message: '提示',
      description: '内内容内容内容内容内容内容内容内容容内容'
    })
  }

  changePlacement = type => {
    const table = {
      'topLeft': '左上',
      'topRight': '右上',
      'bottomLeft': '左下',
      'bottomRight': '右下',
    }
    notification.config({
      placement: type
    })

    message.success(`成功修改notice的位置为${table[type]}`)
  }

  render() {
    return (
      <div className="card-wrap">
        <Card title="改变全局弹出位置">
          <Space>
            <Button type="primary" onClick={ ()=> this.changePlacement('topLeft')}>左上</Button>
            <Button type="primary" onClick={ ()=> this.changePlacement('topRight')}>右上</Button>
            <Button type="primary" onClick={ ()=> this.changePlacement('bottomLeft')}>左下</Button>
            <Button type="primary" onClick={ ()=> this.changePlacement('bottomRight')}>右下</Button>
          </Space>
        </Card>
        <Card title="通知提醒框">
          <Space>
            <Button type="primary" onClick={() => this.openNotification('success')}>Success</Button>
            <Button type="primary" onClick={() => this.openNotification('warning')}>Warning</Button>
            <Button type="primary" onClick={() => this.openNotification('info')}>Info</Button>
            <Button type="primary" onClick={() => this.openNotification('error')}>Error</Button>
          </Space>
        </Card>
      </div>
    )
  }
}