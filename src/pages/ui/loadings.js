import React from 'react'
import { Button, Card, Spin, Space, Alert } from 'antd'
import { SyncOutlined, PlusOutlined } from '@ant-design/icons'

import './ui.less'

export default class Loadings extends React.Component {
  state = {
    isLoading: true
  }

  componentWillMount(){
    setTimeout(() => {
      this.setState({
        isLoading: false
      })
    }, 5000);
  }

  render() {
    // 图标的大小是在 icon组件上定义
    const icon = <SyncOutlined spin style={{ fontSize: 30 }} />
    const icon2 = <PlusOutlined spin />
    return (
      <div className="card-wrap">
        <Card title="Spin用法">
          <Space>
            <Spin size="small" />
            <Spin style={{ marginLeft: '50px' }} />
            <Spin size="large" />
            <Spin indicator={icon} />
            <Spin indicator={icon2} />
          </Space>
        </Card>
        <Card title="内容遮罩层">
          <Alert
            message='React'
            description="欢迎来到React高级实战指南"
            type="info"
            style={{ marginBottom: 10 }}
          />
          <Spin>
            <Alert
              message='React'
              description="欢迎来到React高级实战指南"
              type="success"
              style={{ marginBottom: 10 }}
            />
          </Spin>
          <Spin tip="加载中呢..." spinning={this.state.isLoading}>
            <Alert
              message='React'
              description="欢迎来到React高级实战指南-5秒后loading消失"
              type="success"
              style={{ marginBottom: 10 }}
            />
          </Spin>
        </Card>
      </div>
    )
  }
}