import React from 'react'
import { Button, Card, Radio } from 'antd'
import { PlusOutlined, EditOutlined, DeleteOutlined, SearchOutlined, DownloadOutlined, LeftOutlined, RightOutlined } from '@ant-design/icons'

import './ui.less'


export default class Buttons extends React.Component {
  state = {
    loading: true,
    size: 'default'
  }

  toggleLoading = () => {
    this.setState({
      loading: !this.state.loading
    })
  }

  handleChange = (e) => {
    this.setState({
      size: e.target.value
    })
  }
  render() {
    return (
      <div className="ui-button card-wrap">
        <Card title="基础按钮">
          <Button type="primary">按钮</Button>
          <Button>按钮</Button>
          <Button type="dashed">按钮</Button>
          <Button type="danger">按钮</Button>
          <Button disabled>按钮</Button>
        </Card>
        <Card title="图形按钮">
          <Button icon={<PlusOutlined />}>创建</Button>
          <Button icon={<EditOutlined />}>编辑</Button>
          <Button icon={<DeleteOutlined />}>删除</Button>
          <Button shape="circle" icon={<SearchOutlined />}></Button>
          <Button type="primary" icon={<SearchOutlined />}>搜索</Button>
          <Button type="primary" icon={<DownloadOutlined />}>下载</Button>
        </Card>
        <Card title="Loading按钮">
          <Button loading={this.state.loading}>加载中</Button>
          <Button loading={this.state.loading} shape="circle"></Button>
          <Button type="primary" loading={this.state.loading}>加载</Button>
          <Button type="danger" onClick={this.toggleLoading} >关闭/加载</Button>
        </Card>
        <Card title="按钮组">
          <Button.Group>
            <Button type="primary" icon={<LeftOutlined />}>返回</Button>
            <Button type="primary" icon={<RightOutlined />}>前进</Button>
          </Button.Group>
        </Card>
        <Card title="按钮尺寸">
          <Radio.Group value={this.state.size} onChange={this.handleChange}>
            <Radio value="small">小</Radio>
            <Radio value="default">中</Radio>
            <Radio value="large">大</Radio>
          </Radio.Group>
          <Button type="primary" size={this.state.size}>按钮</Button>
          <Button size={this.state.size}>按钮</Button>
          <Button type="dashed" size={this.state.size}>按钮</Button>
          <Button type="danger" size={this.state.size}>按钮</Button>
          <Button shape="circle" size={this.state.size}>A</Button>
        </Card>
      </div>
    )
  }
}