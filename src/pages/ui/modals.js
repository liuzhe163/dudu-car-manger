import React, { useState } from 'react'
import { Button, Card, Modal } from 'antd'

const Modals = () => {
  const [showList, setShowList] = useState([false, false, false, false]);

  const toggleShow = index => {
    const newList = showList.slice();
    newList.splice(index, 1, !newList[index])
    setShowList(newList)
  }
  let cancel;
  const ok = cancel = index => {
    const newList = showList.slice();
    newList.splice(index, 1, false)
    setShowList(newList)
  }

  const handleConfirm = type => {
    Modal[type]({
      title: '确认?',
      content: '你确定的学会了React了吗?',
      onOk() {
        console.log('Ok');
      },
      onCancel() {
        console.log('Cancel');
      }
    })
  }

  return (
    <div className="ui-modals card-wrap">
      <Card title="基础模态框">
        <Button type="primary" onClick={() => toggleShow(0)}>Open</Button>
        <Button type="primary" onClick={() => toggleShow(1)}>自定义页脚</Button>
        <Button type="primary" onClick={() => toggleShow(2)}>顶部20px弹框</Button>
        <Button type="primary" onClick={() => toggleShow(3)}>水平垂直居中</Button>
      </Card>

      <Card title="信息确认框">
        <Button type="primary" onClick={() => handleConfirm('confirm')}>Confirm</Button>
        <Button type="primary" onClick={() => handleConfirm('info')}>Info</Button>
        <Button type="primary" onClick={() => handleConfirm('success')}>Success</Button>
        <Button type="primary" onClick={() => handleConfirm('warning')}>Warning</Button>
      </Card>

      <Modal
        title="React"
        visible={showList[0]}
        onOk={() => ok(0)}
        onCancel={() => cancel(0)} >
        欢迎使用React组件库AntD的Modal组件
      </Modal>
      <Modal
        title="React"
        visible={showList[1]}
        footer={[
          <Button key="back" onClick={() => ok(1)}>返回关闭</Button>,
          <Button key="submit" type="primary" onClick={() => { cancel(1) }}>确定quit</Button>
        ]}
      >
        欢迎使用React组件库AntD的Modal组件 - 自定义页脚
      </Modal>

      <Modal
        title="Top 20px"
        style={{ top: 20 }}
        visible={showList[2]}
        onOk={() => ok(2)}
        onCancel={() => cancel(2)}>
        欢迎使用React组件库AntD的Modal组件 - top 20px
      </Modal>

      <Modal
        title="居中"
        centered
        visible={showList[3]}
        onOk={() => ok(3)}
        onCancel={() => cancel(3)}>
        欢迎使用React组件库AntD的Modal组件 - centered
      </Modal>
    </div>
  )
}

export default Modals