import React from 'react'
import { Button, Card, Tabs, message } from 'antd'

import { AppleFilled, EditOutlined, PlusOutlined } from '@ant-design/icons'

import './ui.less'

const { TabPane } = Tabs



export default class ITabs extends React.Component {

  state = {
    panes: []
  }

  componentWillMount() {
    const panes = [
      {
        title: '首页',
        content: '首页内容',
        key: '1',
      },
      {
        title: '查询页',
        content: '表格1',
        key: '2'
      },
      {
        title: 'Tab3',
        content: 'Content3-不可删除',
        key: '3',
        closable: false
      }
    ]

    this.setState({
      activeKey: panes[0].key,
      panes
    })
  }

  handleCallback = key => {
    message.success('当前tab是' + key)
  }

  // 自定义的
  onEdit = (targetKey, action) => {
    this[action](targetKey);
  }

  newTabIndex = 4;

  add = () => {
    const panes = this.state.panes;
    const activeKey = `Tab${this.newTabIndex++}`;
    panes.push({ title: activeKey, content: 'New Tab Pane', key: activeKey });
    this.setState({ panes, activeKey });
  }
  remove = targetKey => {
    const { panes, activeKey } = this.state;
    let newActiveKey = activeKey;
    let lastIndex;
    panes.map((pane, index) => {
      if (pane.key === targetKey) {
        lastIndex = index - 1;
      }
    })

    const newPanes = panes.filter(pane => pane.key !== targetKey);
    if (newPanes.length && newActiveKey === targetKey) {
      if (lastIndex >= 0) {
        newActiveKey = newPanes[lastIndex].key;
      } else {
        newActiveKey = newPanes[0].key
      }
    }

    this.setState({
      panes: newPanes,
      activeKey: newActiveKey
    })
  }

  onChange = activeKey => {
    this.setState({ activeKey });
  };
  // 自定义的end

  render() {
    return (
      <div className="card-wrap">
        <Card title="Tab页签">
          <Tabs defaultActiveKey="1" onChange={this.handleCallback}>
            <TabPane tab="Tab 1" key="1">
              欢迎使用React
            </TabPane>
            <TabPane tab="Tab 2" key="2">
              React是一个很好的MV*框架
            </TabPane>
            <TabPane tab="Tab 3" key="3">
              welcome to React antD
            </TabPane>
          </Tabs>
        </Card>
        <Card title="带图的页签">
          <Tabs defaultActiveKey="1" onChange={this.handleCallback}>
            <TabPane tab={<span><AppleFilled />logo</span>} key="1">
              欢迎使用React
            </TabPane>
            <TabPane tab={<span><PlusOutlined />添加</span>} key="2">
              React是一个很好的MV*框架
            </TabPane>
            <TabPane tab={<span><EditOutlined />编辑</span>} key="3">
              welcome to React antD
            </TabPane>
          </Tabs>
        </Card>
        <Card title="动态tab">
          <div style={{ marginBottom: '8px' }}>
            <Button type="dashed" shape="circle-outline" onClick={this.add}>+</Button>
          </div>
          <Tabs
            hideAdd
            type="editable-card"
            activeKey={this.state.activeKey}
            onEdit={this.onEdit}
            onChange={this.onChange}
          >
            {
              this.state.panes.map(pane => {
                return (
                  <TabPane
                    closable={pane.closable}
                    tab={pane.title}
                    key={pane.key}>
                    {pane.content}
                  </TabPane>
                )
              })
            }
          </Tabs>
        </Card>
      </div>
    )
  }
}