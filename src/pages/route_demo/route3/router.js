import React, { Component } from 'react'
import { HashRouter as Router, Route, Switch } from 'react-router-dom'

import Main from './Main'

import About from '../route1/components/About'
import Topic from '../route1/components/Topic'
import NoMatch from '../route1/components/NoMatch'

import { Info } from './Info'

import Home from './Home'

export default class IRouter extends Component {

  render() {
    return (
      <Router>
        <Home>
          <Switch>

            <Route path="/main" render={() => {
              return (
                <Main>
                  <Route path="/main/:id" component={Info}></Route>
                </Main>)
            }}></Route>
            <Route path="/about" component={About}></Route>
            <Route path="/topics" component={Topic}></Route>
            <Route path="*" component={NoMatch}></Route>

          </Switch>
        </Home>
      </Router>
    )
  }
}