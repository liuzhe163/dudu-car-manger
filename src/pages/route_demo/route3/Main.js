import React from 'react'

import { Link } from 'react-router-dom'
export default function Main(props){
  return (
    <div>
      Main page
      <Link to="/main/a">嵌套子路由</Link>
      {props.children}
      <hr/>
    </div>
  )
}