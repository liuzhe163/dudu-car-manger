import React, { Component } from 'react'
import { Link } from 'react-router-dom'


export default class Home extends Component {
  render() {
    return (
      <div>
        <h1>动态路由演示</h1>
        <ul>
          <li><Link to="/main">main1</Link></li>
          <li><Link to="/about">about1</Link></li>
          <li><Link to="/topics">topic1</Link></li>

          <hr/>
          <li><Link to="/main/123">id123</Link></li>
          <li><Link to="/main/456">id456</Link></li>

        </ul>
        <hr/>
        <div>
          {this.props.children}
        </div>
      </div>
    )
  }
}
