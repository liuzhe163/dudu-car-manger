import React, {Component} from 'react'
import {HashRouter, Route, Link, Switch} from 'react-router-dom'

import Main from './components/Main'
import About from './components/About'
import Topic from './components/Topic'

export default class Home extends Component {
  render(){
    return (
      <HashRouter>
        <div>
          <ul>
            <li><Link to="/">main</Link></li>
            <li><Link to="/about">about</Link></li>
            <li><Link to="/topics">topic</Link></li>
          </ul>
          <Switch>
            <Route path="/about" component={About}></Route>
            <Route path="/topics" component={Topic}></Route>
            <Route path="/" component={Main}></Route>
          </Switch>
        </div>
      </HashRouter>
    )
  }
}
