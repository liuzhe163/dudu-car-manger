import React, { Component } from 'react'
import { Link } from 'react-router-dom'


export default class Home extends Component {
  render() {
    return (
      <div>
        <ul>
          <li><Link to="/main">main1</Link></li>
          <li><Link to="/about">about1</Link></li>
          <li><Link to="/topics">topic1</Link></li>
        </ul>
        <hr/>
        <div>
          {this.props.children}
        </div>
      </div>
    )
  }
}
