import React from 'react';

import {
  Card,
  Form,
  Button,
  Input,
  Checkbox,
  Radio,
  InputNumber,
  Select,
  Switch,
  DatePicker,
  TimePicker,
  Upload,
  message,
} from 'antd';
// import TextArea from 'antd/lib/input/TextArea';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';

import moment from 'moment';

const FormItem = Form.Item;
const { Option } = Select;
const { TextArea } = Input;

// 处理file文件成base64类型图像格式
function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}
export default class FormRegister extends React.Component {
  state = {
    loading: false,
  };
  formRef = React.createRef();
  handleSubmit = () => {
    this.formRef.current
      .validateFields()
      .then((userInfo) => {
        console.log('123213');
        console.log(userInfo);
        message.success(
          `用户名:${userInfo.username} 密码:${userInfo.password}`
        );
      })
      .catch((err) => {
        console.log('catch错误', err);
      });
  };
  uploadChange = (info) => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, (imageUrl) =>
        this.setState({
          imageUrl,
          loading: false,
        })
      );
    }
  };
  render() {
    const formItemLayout = {
      labelCol: {
        xs: 24,
        sm: 6,
      },
      wrapperCol: {
        xs: 24,
        sm: 18,
      },
    };
    const dateFormat = 'YYYY/MM/DD';
    const { loading, imageUrl } = this.state;
    const uploadButton = (
      <div>
        {loading ? <LoadingOutlined /> : <PlusOutlined />}
        <div style={{ marginTop: 8 }}>上传</div>
      </div>
    );
    return (
      <div className="card-wrap">
        <Card title="注册">
          <Form
            ref={this.formRef}
            {...formItemLayout}
            initialValues={{
              gender: 'male',
              age: 19,
              marry: true,
              state: '1',
              hobby: ['1'],
              birthday: moment('1995-12-10'),
              address: '北京市海淀区奥林匹克公园',
              read: true,
            }}
            style={{ width: 500, marginLeft: 100 }}
          >
            <FormItem
              label="姓名"
              name="username"
              rules={[{ required: true, message: '用户名必填' }]}
            >
              <Input placeholder="请输入用户名" />
            </FormItem>
            <FormItem
              label="密码"
              name="password"
              rules={[{ required: true, message: '密码必填' }]}
            >
              <Input type="password" placeholder="请输入密码" />
            </FormItem>
            <FormItem label="性别" name="gender">
              <Radio.Group>
                <Radio value="male">男</Radio>
                <Radio value="female">女</Radio>
              </Radio.Group>
            </FormItem>
            <FormItem label="年龄" name="age">
              <InputNumber />
            </FormItem>
            <FormItem label="当前状态" name="state">
              <Select placeholder="请选择当前状态">
                <Option key="1">111</Option>
                <Option key="2">222</Option>
                <Option key="3">333</Option>
              </Select>
            </FormItem>
            <FormItem label="爱好" name="hobby">
              <Select
                mode="multiple"
                placeholder="请选择你的爱好"
                // defaultValue={['1', '2']}
              >
                <Option key="1">跑步</Option>
                <Option key="2">游泳</Option>
                <Option key="3">打球</Option>
              </Select>
            </FormItem>
            <FormItem label="是否已婚" name="marry" valuePropName="checked">
              <Switch />
            </FormItem>
            <FormItem label="生日" name="birthday">
              <DatePicker showNow format={dateFormat} />
            </FormItem>
            <FormItem label="联系地址" name="address">
              <TextArea autoSize={{ minRows: 2, maxRows: 5 }} />
            </FormItem>
            <FormItem label="早起时间" name="getUp">
              <TimePicker />
            </FormItem>
            <FormItem label="头像" name="avatar">
              <Upload
                listType="picture-card"
                showUploadList={false}
                action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                // beforeUpload={beforeUpload}
                onChange={this.uploadChange}
              >
                {imageUrl ? (
                  <img src={imageUrl} alt="avatar" style={{ width: '100%' }} />
                ) : (
                  uploadButton
                )}
              </Upload>
            </FormItem>
            <FormItem
              name="read"
              wrapperCol={{ offset: 6, span: 16 }}
              valuePropName="checked"
            >
              <Checkbox>
                <span>&nbsp;&nbsp;</span>
                我已阅读过<a href="#">慕课协议</a>
              </Checkbox>
            </FormItem>
            <FormItem wrapperCol={{ offset: 6, span: 16 }}>
              <Button type="primary" onClick={this.handleSubmit}>
                注册
              </Button>
            </FormItem>
          </Form>
        </Card>
      </div>
    );
  }
}
