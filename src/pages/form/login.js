import React from 'react'

import { Card, Form, Input, Button, message, Checkbox } from 'antd'


const FormItem = Form.Item;
export default class FormLogin extends React.Component {
  formRef = React.createRef();
  onSubmit = () => {
    this.formRef.current.validateFields().then(userInfo => {
      message.success(`用户名:${userInfo.username} 密码:${userInfo.password}`)
    }).catch((err) => {
      console.log('catch错误', err);
    });
  }
  render() {
    return (
      <div className="card-wrap">
        <Card title="行内登录表单">
          <Form layout="inline">
            <FormItem
              label="用户名"
              name="username"
              rules={[
                {
                  required: true,
                  message: '用户名必填'
                }
              ]}
            >
              <Input placeholder="请输入用户名" />
            </FormItem>
            <FormItem
              label="密码"
              name="password"
              rules={[
                {
                  required: true,
                  message: '密码必填'
                }
              ]}
            >
              <Input.Password placeholder="请输入密码" />
            </FormItem>
            <FormItem>
              <Button type="primary" htmlType="submit">登录</Button>
            </FormItem>
          </Form>
        </Card>
        <Card title="水平登录表单">
          <Form ref={this.formRef}
            initialValues={{ remember: true }}
            style={{ width: 300 }}>
            <FormItem
              label="用户名"
              name="username"
              rules={[
                {
                  min: 4,
                  max: 10,
                  message: '必须4-10'
                },
                {
                  pattern: /^\w/g,
                  message: '只能是字母和数字'
                }
              ]} >
              <Input placeholder="请输入用户名" />
            </FormItem>
            <FormItem
              label="密码"
              name="password"
              rules={[
                {
                  required: true,
                  message: '密码必填'
                }
              ]}
            >
              <Input type="password" placeholder="请输入密码" />
            </FormItem>
            <FormItem name="remember" valuePropName="checked">
              <Checkbox>记住密码</Checkbox>
              <a href="http://www.baidu.com" target="blank" style={{float: "right"}}>忘记密码</a>
            </FormItem>
            <FormItem>
              <Button type="primary" onClick={this.onSubmit}>登录</Button>
            </FormItem>
          </Form>
        </Card>
      </div>
    )
  }
}
