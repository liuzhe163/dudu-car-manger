import React, { Component } from 'react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';

import App from './App';
import Login from './pages/login';
import Admin from './admin';

// ui
import Buttons from './pages/ui/buttons';
import Modals from './pages/ui/modals';
import Loadings from './pages/ui/loadings';
import Notice from './pages/ui/notice';
import Messages from './pages/ui/messages';
import Tabs from './pages/ui/tabs';
import Gallery from './pages/ui/gallery';
import Carousel from './pages/ui/carousel';

// 表单
import FormLogin from './pages/form/login';
import FormRegister from './pages/form/register';

// 表格
import TableBasic from './pages/table/basic';
import TableHigh from './pages/table/high';

// 城市管理
import City from './pages/city/index';
// 订单管理
import Order from './pages/order/index';
import OrderDetail from './pages/order/detail';

import Common from './common';

import NoMatch from './pages/noMatch';

export default class IRouter extends Component {
  render() {
    return (
      <Router>
        <App>
          <Switch>
            <Route path="/login" component={Login}></Route>
            <Route
              path="/order/detail"
              render={() => <Common>{<OrderDetail />}</Common>}
            ></Route>
            <Route
              path="/admin"
              render={() => {
                return (
                  <Admin>
                    <Switch>
                      <Route path="/admin/ui/buttons" component={Buttons} />
                      <Route path="/admin/ui/modals" component={Modals} />
                      <Route path="/admin/ui/loadings" component={Loadings} />
                      <Route path="/admin/ui/notification" component={Notice} />
                      <Route path="/admin/ui/messages" component={Messages} />
                      <Route path="/admin/ui/tabs" component={Tabs} />
                      <Route path="/admin/ui/gallery" component={Gallery} />
                      <Route path="/admin/ui/carousel" component={Carousel} />
                      <Route component={NoMatch} />
                    </Switch>
                  </Admin>
                );
              }}
            ></Route>
            <Route
              path="/"
              render={() => {
                return (
                  <Admin>
                    <Switch>
                      <Route path="/form/login" component={FormLogin} />
                      <Route path="/form/reg" component={FormRegister} />
                      <Route path="/table/basic" component={TableBasic} />
                      <Route path="/table/high" component={TableHigh} />
                      <Route path="/city" component={City} />
                      <Route path="/order" component={Order} />
                    </Switch>
                  </Admin>
                );
              }}
            ></Route>
          </Switch>
        </App>
      </Router>
    );
  }
}
