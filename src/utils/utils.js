import React from 'react';

export default {
  formateDate(time) {
    if (!time) return '';
    let date = new Date(time);
    return `${date.getFullYear()}-${
      date.getMonth() + 1
    }-${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
  },
  pagination(data, callback) {
    return {
      onChange: (current) => {
        callback(current);
      },
      current: data.result.current,  //todo 这里边应该使用 data.result.page
      pageSize: data.result.pageSize,
      total: data.result.total,
      showTotal: () => `共${data.result.total}条`,
    };
  },
};
