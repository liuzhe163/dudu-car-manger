import JsonP from 'jsonp';
import axios from 'axios';
import { notification } from 'antd';
import Utils from '../utils/utils'

export default class Axios {
  static requestList(_this,url,params) {
    console.log('通过requestList调用')
    const data = {
      params
    }
    this.ajax({
      url,
      data
    }).then(data=>{
        const list = data.result.item_list;
        _this.setState({
          list: list.map((item) => ({
            ...item,
            key: item.id,
          })),
          pagination: Utils.pagination(data, (current) => {
            _this.params.page = current;
            _this.request();
          }),
        });
    })
  }
  static jsonp(options) {
    return new Promise((resolve, reject) => {
      JsonP(
        options.url,
        {
          param: 'callback',
        },
        function (err, response) {
          console.log('jsonp返回');
          console.log(arguments);
          if (response.status === 'success') {
            resolve(response);
          } else {
            reject(response.message);
          }
        }
      );
    });
  }

  static ajax(options) {
    if (options.data && options.data.loading !== false) {
      console.log('loading');
      document.getElementById('ajaxLoading').style.display = 'block';
    }
    return new Promise((resolve, reject) => {
      axios({
        url: options.url,
        method: 'get',
        // baseURL: 'https://easy-mock.com/mock/5fbe13a51bc0682deacbfef8/api',
        baseURL: 'https://www.fastmock.site/mock/a2998efe941f9e17cc811fc9960af66e/api',
        timeout: 10000,
        params: (options.data && options.data.params) || '',
      }).then((response) => {
        if (options.data && options.data.loading !== false) {
          document.getElementById('ajaxLoading').style.display = 'none';
        }
        if (response.status === 200) {
          let res = response.data;
          if (res.code === 0) {
            resolve(res);
          } else {
            notification.info({
              message: '提示',
              description: res.message,
            });
          }
        } else {
          reject(response.data);
        }
      });
    });
  }
}
